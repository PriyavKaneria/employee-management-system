from flask import Blueprint, render_template, redirect, url_for, request, flash, Response
from flask.helpers import make_response
from werkzeug.security import generate_password_hash, check_password_hash
# from flask_login import login_user, logout_user, login_required
from .app import db
from .models.models import User, Roles, Employees
from datetime import datetime as dt
from datetime import timedelta as td
from flask_jwt_extended import create_access_token
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required

auth = Blueprint('auth', __name__)

@auth.route('/login')
def login():
    return render_template('login.html')

@auth.route('/login', methods=['POST'])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = User.get_email(email)

    if (not user) or (not check_password_hash(user.password, password)):
        flash('Please check your login details and try again.')
        return {"message": "Invalid password", "status_code": 401}

    if not user.is_active:
        flash('Your account is inactive')
        return {"message": "Inactive account", "status_code": 401}
    
    access_token = create_access_token(identity=user.id, expires_delta=td(hours=1))
    resp = Response()
    resp.set_cookie("Authorization", "Bearer " + access_token)
    return resp

@auth.route('/signup')
def signup():
    return render_template('signup.html')

@auth.route('/managers')
def get_managers():
    users = list(Roles.get("Manager").users)
    return {"users": users}

@auth.route('/employees')
def get_employees():
    users = Roles.get("Employee").users
    ser_users=[]
    for user in users:
        ser_users.append(user.as_dict())
    return {"users": ser_users}

@auth.route('/signup', methods=['POST'])
def signup_post():
    email = request.form.get('email')
    name = request.form.get('name')
    contact = request.form.get('contact')
    role = request.form.get('role')
    employees = ''
    manager = ''
    if role == 'Manager':
        employees = request.form.get('employees')
    elif role == 'Employee':
        manager = request.form.get('manager')
    password = request.form.get('password')

    user = User.get_email(email)

    if user:
        flash('Email address already exists.')
        # return redirect(url_for('auth.signup'))
        return {"message": "Email already exists", "status_code": 401}
    
    now = dt.now()
    new_user = User(email=email, name=name, password=generate_password_hash(password, method='sha256'), contact_no=contact, created_on=now, updated_on=now, is_active=True, role=role)
    new_user.save()
    db.session.commit()
    if role == 'Employee':
        if manager == "":
            employee = Employees(id=new_user.id, manager_id=None)
        else:
            employee = Employees(id=new_user.id, manager_id=int(manager))
        employee.save()
    elif role == 'Manager':
        for employee in employees:
            emp = Employees.get(employee)
            emp.manager_id = new_user.id
    db.session.commit()

    # return redirect(url_for('auth.login'))
    return {"message": "Successfully signed up", "status_code": 200}

@auth.route('/logout', methods=['POST'])
@jwt_required()
def logout():
    # return redirect(url_for('main.index'))
    resp = Response()
    resp.delete_cookie("Authorization")
    return resp