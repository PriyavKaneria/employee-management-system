import Head from "next/head"
import Image from "next/image"
import styles from "../styles/Home.module.css"
import cookies from "next-cookies"

export async function getServerSideProps(context) {
	const { Authorization } = cookies(context)
	const res = await fetch(`http://localhost:5000/`, {
		headers: { Authorization: Authorization },
	})
	if (res.status != 200) {
		return {
			props: {
				is_authenticated: false,
			},
		}
	}
	return {
		props: {
			is_authenticated: true,
		},
	}
}

export default function Home({ is_authenticated }) {
	console.log(is_authenticated)
	return (
		<div className={styles.container}>
			<Head>
				<title>Employee Management System</title>
				<meta name='description' content='CRUD and Authentication App' />
				<link rel='icon' href='/favicon.ico' />
				<link
					rel='stylesheet'
					href='https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css'
				/>
			</Head>

			<section className='hero is-primary is-fullheight'>
				<div className='hero-head'>
					<nav className='navbar'>
						<div className='container'>
							<a
								role='button'
								class='navbar-burger'
								data-target='navMenu'
								aria-label='menu'
								aria-expanded='false'>
								<span aria-hidden='true'></span>
								<span aria-hidden='true'></span>
								<span aria-hidden='true'></span>
							</a>
							<div id='navbarMenuHeroA' className='navbar-menu is-active'>
								<div className='navbar-end'>
									<a href='/' className='navbar-item'>
										Home
									</a>
									{is_authenticated && (
										<a href='/profile' className='navbar-item'>
											Profile
										</a>
									)}
									{is_authenticated && (
										<a href='/tasks' className='navbar-item'>
											Tasks
										</a>
									)}
									{!is_authenticated && (
										<a href='/login' className='navbar-item'>
											Login
										</a>
									)}
									{!is_authenticated && (
										<a href='/signup' className='navbar-item'>
											Sign Up
										</a>
									)}
									{is_authenticated && (
										<a href='/logout' className='navbar-item'>
											Logout
										</a>
									)}
								</div>
							</div>
						</div>
					</nav>
				</div>

				<div className='hero-body'>
					<div className='container has-text-centered'>
						<h1 class='title'>Employee Management System using Flask+NextJs</h1>
						<h2 class='subtitle'>Hope you like it!</h2>
					</div>
				</div>
			</section>
		</div>
	)
}
