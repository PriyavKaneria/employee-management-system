# Employee Management System
## Made using NextJs and Flask with SQLitedb

### Steps to Run project

1. Clone Project
2. Install required dependencies for frontend as well as backend
3. Run the frontend server using yarn dev
4. Migrate the models in the application folder using python command `db.create_all(app=create_app())` after importing db and create_app from app.py
5. Run the backend server using `FLASK_APP=application` and `flask run`

### Tested application on Windows