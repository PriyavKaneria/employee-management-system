import Head from "next/head"
// import styles from "../styles/Profile.module.css"
import { useState } from "react"
import cookies from "next-cookies"

export async function getServerSideProps(context) {
	const { Authorization } = cookies(context)
    const { id } = context.query
	const res = await fetch(`http://localhost:5000/task/${id}`, {
		headers: { Authorization: Authorization }
	})
	if (res.status != 200) {
		return {
			props: {
				is_authenticated: false,
			},
		}
	}
	const data = await res.json()
    
	const resp = await fetch(`http://localhost:5000/employees`)
	const data2 = await resp.json()
	return {
		props: {
			is_authenticated: true,
			task: data.task,
            employees: data2.users
		},
	}
}

export default function NewTask({ is_authenticated, task, employees }, context) {
	const { Authorization } = cookies(context)
	const [title, setTitle] = useState(task.title)
	const [description, setDescription] = useState(task.description)
	const [deadline, setDeadline] = useState(task.deadline)
	const [status, setStatus] = useState(task.status)
	const [employee, setEmployee] = useState(task.user_id)
	const [showMessage, setShowMessage] = useState(false)
	const [invalidTitle, setInvalidTitle] = useState(false)
	const [invalidDescription, setInvalidDescription] = useState(false)
	const [invalidDeadline, setInvalidDeadline] = useState(false)
	function handleSubmit(e) {
		e.preventDefault()
		if (!Validate(title)) {
			setInvalidTitle(true)
			return
		} else {
			setInvalidTitle(false)
		}
		if (!Validate(description)) {
			setInvalidDescription(true)
			return
		} else {
			setInvalidDescription(false)
		}
		if (!Validate(deadline)) {
			setInvalidDeadline(true)
			return
		} else {
			setInvalidDeadline(false)
		}
		setShowMessage(false)
		var formData = new FormData()
        formData.append("task_id", task.id)
		formData.append("title", title)
		formData.append("description", description)
		formData.append("deadline", deadline)
		formData.append("status", status)
		formData.append("employee", employee)
		fetch("http://localhost:5000/updatetask", {
			method: "POST",
			body: formData,
			headers: { Authorization: Authorization },
		})
			.then((res) => {
				return res
			})
			.then((res) => {
				// console.log(res)
				if (res.status == 200) {
					window.location = "/tasks"
				} else setShowMessage(true)
				return res
			})
	}

	function Validate(string) {
		return string.trim() != ""
	}
	return (
		<div className="mycontainer">
			<Head>
				<title>Employee Management System</title>
				<meta name='description' content='CRUD and Authentication App' />
				<link rel='icon' href='/favicon.ico' />
				<link
					rel='stylesheet'
					href='https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css'
				/>
			</Head>

			<section className='hero is-primary is-fullheight'>
				<div className='hero-head'>
					<nav className='navbar'>
						<div className='container'>
							<a
								role='button'
								class='navbar-burger'
								data-target='navMenu'
								aria-label='menu'
								aria-expanded='false'>
								<span aria-hidden='true'></span>
								<span aria-hidden='true'></span>
								<span aria-hidden='true'></span>
							</a>
							<div id='navbarMenuHeroA' className='navbar-menu is-active'>
								<div className='navbar-end'>
									<a href='/' className='navbar-item'>
										Home
									</a>
									{is_authenticated && (
										<a href='/profile' className='navbar-item'>
											Profile
										</a>
									)}
									{is_authenticated && (
										<a href='/tasks' className='navbar-item'>
											Tasks
										</a>
									)}
									{!is_authenticated && (
										<a href='/login' className='navbar-item'>
											Login
										</a>
									)}
									{!is_authenticated && (
										<a href='/signup' className='navbar-item'>
											Sign Up
										</a>
									)}
									{is_authenticated && (
										<a href='/logout' className='navbar-item'>
											Logout
										</a>
									)}
								</div>
							</div>
						</div>
					</nav>
				</div>

				<div className='hero-body'>
					<div
						className={
							"container has-text-centered overridecontainer"
						}>
						<div className='column is-6 is-offset-3'>
							<h3 className='title'>Update Task</h3>
							<div className='box'>
								{showMessage && (
									<div className='notification is-danger'>Invalid details</div>
								)}
								<form onSubmit={handleSubmit}>
									<div class='field'>
										<div class='control'>
											<input
												class='input is-large'
												type='text'
												name='title'
												placeholder='Task title'
                                                value={title}
												autofocus=''
												onChange={(e) => {
													setTitle(e.target.value)
												}}
											/>
											{invalidTitle && (
												<p className="dangerText">
													You have entered an invalid task title!
												</p>
											)}
										</div>
									</div>

									<div class='field'>
										<div class='control'>
											<input
												class='input is-large'
												type='text'
                                                value={description}
												name='description'
												placeholder='Task Description'
												onChange={(e) => {
													setDescription(e.target.value)
												}}
											/>
											{invalidDescription && (
												<p className="dangerText">
													Please enter a proper description!
												</p>
											)}
										</div>
									</div>

									<div class='field'>
										<div class='control'>
											<input
												class='input is-large'
												type='date'
                                                value={deadline}
												name='deadline'
												placeholder='Task deadline'
												onChange={(e) => {
													setDeadline(e.target.value)
												}}
											/>
											{invalidDeadline && (
												<p className="dangerText">
													Please enter a proper deadline!
												</p>
											)}
										</div>
									</div>

									<div class='field'>
										<div class='control'>
											<select
												className='select is-large is-fullwidth'
												onChange={(e) => {
													setStatus(e.target.value)
												}}>
												<option value='Todo'>Todo</option>
												<option value='Doing'>Doing</option>
												<option value='Completed'>Completed</option>
											</select>
										</div>
									</div>

									<div className='field'>
										<div className='control'>
											<div className='select is-large is-fullwidth'>
												<select
													onChange={(e) => {
														setEmployee(e.target.value)
													}}>
													{employees.map((emp) => {
														return <option value={emp.id}>{emp.name}</option>
													})}
												</select>
											</div>
										</div>
									</div>

									<button class='button is-block is-info is-large is-fullwidth'>
										Update Task
									</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	)
}
