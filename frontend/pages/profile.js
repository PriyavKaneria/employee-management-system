import Head from "next/head"
import styles from "../styles/Profile.module.css"
import { useState } from "react"
import cookies from "next-cookies"

export async function getServerSideProps(context) {
	const { Authorization } = cookies(context)
	const res = await fetch(`http://localhost:5000/profile`, {
		headers: { Authorization: Authorization },
	})
	if (res.status != 200) {
		return {
			props: {
				is_authenticated: false,
			},
		}
	}
	const data = await res.json()
	return {
		props: {
			is_authenticated: true,
			name: data.name,
			users: data.users,
		},
	}
}

export default function Profile({ is_authenticated, name, users }, context) {
	const { Authorization } = cookies(context)
	function Activate(id) {
		fetch("http://localhost:5000/activate/" + id, {
			headers: { Authorization: Authorization },
		}).then((res) => {
			window.location = "/profile"
		})
	}
	function Deactivate(id) {
		fetch("http://localhost:5000/deactivate/" + id, {
			headers: { Authorization: Authorization },
		}).then((res) => {
			window.location = "/profile"
		})
	}
	const user_data = users.map((user) => {
		return (
			<tr>
				<td>{user.name}</td>
				<td>{user.email}</td>
                <td>{user.contact_no}</td>
				<td>{user.created_on}</td>
				<td>{user.updated_on}</td>
				<td>{user.role}</td>
				<td>
					{user.is_active && (
						<a onClick={(e) => Deactivate(user.id)}>
							<button className="button is-small is-danger">Deactivate</button>
						</a>
					)}
					{!user.is_active && (
						<a onClick={(e) => Activate(user.id)}>
							<button className="button is-small is-success">Activate</button>
						</a>
					)}
				</td>
			</tr>
		)
	})
	return (
		<div className={styles.container}>
			<Head>
				<title>Employee Management System</title>
				<meta name='description' content='CRUD and Authentication App' />
				<link rel='icon' href='/favicon.ico' />
				<link
					rel='stylesheet'
					href='https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css'
				/>
			</Head>

			<section className='hero is-primary is-fullheight'>
				<div className='hero-head'>
					<nav className='navbar'>
						<div className='container'>
<a role="button" class="navbar-burger" data-target="navMenu" aria-label="menu" aria-expanded="false">
								<span aria-hidden="true"></span>
								<span aria-hidden="true"></span>
								<span aria-hidden="true"></span>
							</a>
							<div id='navbarMenuHeroA' className='navbar-menu is-active'>
								<div className='navbar-end'>
									<a href="/" className='navbar-item'>
										Home
									</a>
									{is_authenticated && (
										<a href='/profile' className='navbar-item'>
											Profile
										</a>
									)}
									{is_authenticated && (
										<a href='/tasks' className='navbar-item'>
											Tasks
										</a>
									)}
									{!is_authenticated && (
										<a href='/login' className='navbar-item'>
											Login
										</a>
									)}
									{!is_authenticated && (
										<a href='/signup' className='navbar-item'>
											Sign Up
										</a>
									)}
									{is_authenticated && (
										<a href='/logout' className='navbar-item'>
											Logout
										</a>
									)}
								</div>
							</div>
						</div>
					</nav>
				</div>

				<div className='hero-body'>
					<div
						className={
							"container has-text-centered " + styles.overridecontainer
						}>
						<h1 className={"title " + styles.greeting}>Welcome, {name}!!!</h1>
						<a className={styles.UpdateButton} href='/update'>
							<button>Update Your Profile</button>
						</a>
						<div class={styles.tblheader}>
							<table cellpadding='0' cellspacing='0' border='0'>
								<thead>
									<tr>
										<th>Name</th>
										<th>Email</th>
                                        <th>Contact No</th>
										<th>Created On</th>
										<th>Updated On</th>
										<th>Role</th>
										<th>Actions</th>
									</tr>
								</thead>
							</table>
						</div>
						<div class={styles.tblcontent}>
							<table cellpadding='0' cellspacing='0' border='0'>
								<tbody>{user_data}</tbody>
							</table>
						</div>
					</div>
				</div>
			</section>
		</div>
	)
}
