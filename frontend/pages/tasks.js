import Head from "next/head"
import styles from "../styles/Profile.module.css"
import { useState } from "react"
import cookies from "next-cookies"

export async function getServerSideProps(context) {
	const { Authorization } = cookies(context)
	const res = await fetch(`http://localhost:5000/tasks`, {
		headers: { Authorization: Authorization },
	})
	if (res.status != 200) {
		return {
			props: {
				is_authenticated: false,
			},
		}
	}
	const data = await res.json()
	return {
		props: {
			is_authenticated: true,
			is_admin: data.is_admin,
			tasks: data.tasks,
		},
	}
}

export default function Tasks({ is_authenticated, is_admin, tasks }, context) {
	if (!tasks) tasks = [{}]
	if (!is_admin) is_admin = false
	const { Authorization } = cookies(context)
	function UpdateTaskStatus(task_id, status) {
		var formData = new FormData()
		formData.append("task_id", task_id)
		formData.append("status", status)
		fetch("http://localhost:5000/changestatus", {
			method: "POST",
			body: formData,
			headers: { Authorization: Authorization },
		})
			.then((res) => {
				return res
			})
			.then((res) => {
				// console.log(res)
				if (res.status == 200) {
					window.location = "/tasks"
				}
				return res
			})
	}
	
	function DeleteTask(task_id) {
		var formData = new FormData()
		formData.append("task_id", task_id)
		fetch("http://localhost:5000/deletetask", {
			method: "POST",
			body: formData,
			headers: { Authorization: Authorization },
		})
			.then((res) => {
				return res
			})
			.then((res) => {
				// console.log(res)
				if (res.status == 200) {
					window.location = "/tasks"
				}
				return res
			})
	}
	const task_data = tasks.map((task) => {
		return (
			<tr>
				<td>{task.assigned}</td>
				<td>{task.title}</td>
				<td>{task.description}</td>
				<td>{task.deadline}</td>
				<td>
					<select
						className='select is-small is-info'
						value={task.status}
						onChange={(e) => {
							UpdateTaskStatus(task.id, e.target.value)
						}}>
						<option value='Todo'>Todo</option>
						<option value='Doing'>Doing</option>
						<option value='Completed'>Completed</option>
					</select>
				</td>
				{is_admin && (
					<td>
						<a href={`/task/${task.id}`}><button className='button is-warning is-small'>Edit</button></a>&nbsp;
						<button className='button is-danger is-small' onClick={(e) => {DeleteTask(task.id)}}>Delete</button>
					</td>
				)}
			</tr>
		)
	})
	return (
		<div className={styles.container}>
			<Head>
				<title>Employee Management System</title>
				<meta name='description' content='CRUD and Authentication App' />
				<link rel='icon' href='/favicon.ico' />
				<link
					rel='stylesheet'
					href='https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css'
				/>
			</Head>

			<section className='hero is-primary is-fullheight'>
				<div className='hero-head'>
					<nav className='navbar'>
						<div className='container'>
							<a
								role='button'
								class='navbar-burger'
								data-target='navMenu'
								aria-label='menu'
								aria-expanded='false'>
								<span aria-hidden='true'></span>
								<span aria-hidden='true'></span>
								<span aria-hidden='true'></span>
							</a>
							<div id='navbarMenuHeroA' className='navbar-menu is-active'>
								<div className='navbar-end'>
									<a href='/' className='navbar-item'>
										Home
									</a>
									{is_authenticated && (
										<a href='/profile' className='navbar-item'>
											Profile
										</a>
									)}
									{is_authenticated && (
										<a href='/tasks' className='navbar-item'>
											Tasks
										</a>
									)}
									{!is_authenticated && (
										<a href='/login' className='navbar-item'>
											Login
										</a>
									)}
									{!is_authenticated && (
										<a href='/signup' className='navbar-item'>
											Sign Up
										</a>
									)}
									{is_authenticated && (
										<a href='/logout' className='navbar-item'>
											Logout
										</a>
									)}
								</div>
							</div>
						</div>
					</nav>
				</div>

				<div className='hero-body'>
					<div
						className={
							"container has-text-centered " + styles.overridecontainer
						}>
						<h1 className={"title " + styles.greeting}>Tasks</h1>
						{is_admin && (
							<a className={styles.UpdateButton} href='/newtask'>
								<button>Create new Task</button>
							</a>
						)}
						<div class={styles.tblheader}>
							<table cellpadding='0' cellspacing='0' border='0'>
								<thead>
									<tr>
										<th>Assigned to</th>
										<th>Title</th>
										<th>Description</th>
										<th>Deadline</th>
										<th>Status</th>
										{is_admin && <th>Actions</th>}
									</tr>
								</thead>
							</table>
						</div>
						<div class={styles.tblcontent}>
							<table cellpadding='0' cellspacing='0' border='0'>
								<tbody>{task_data}</tbody>
							</table>
						</div>
					</div>
				</div>
			</section>
		</div>
	)
}
