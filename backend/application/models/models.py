# from flask_login import UserMixin
from ..app import db
from datetime import datetime as dt

class Roles(db.Model):
    __table_args__ = {'extend_existing': True}
    role = db.Column(db.String(100), primary_key=True)
    users = db.relationship('User', backref='roles', lazy='dynamic')
    
    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}
       
    @classmethod
    def get(cls, id):
        return cls.query.get(id)

class User(db.Model):
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))
    contact_no = db.Column(db.Unicode(255))
    created_on = db.Column(db.DateTime)
    updated_on = db.Column(db.DateTime)
    is_active = db.Column(db.Boolean)
    role = db.Column(db.String(100), db.ForeignKey('roles.role'))
    task = db.relationship('Tasks', backref='user', lazy='dynamic')
    
    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        self.is_active = False
        db.session.commit()

    def undelete(self):
        self.is_active = True
        db.session.commit()

    def update(self, body):
        for x in body:
            if x == 'email':
                self.email = body[x]
            elif x == 'phone':
                self.phone = body[x]
            elif x == 'name':
                self.name = body[x]
        self.updatedAt = dt.now()
        db.session.commit()

    @classmethod
    def get(cls, id):
        return cls.query.get(id)

    @classmethod
    def get_email(cls, email):
        return cls.query.filter_by(email=email).first()
        
    @classmethod
    def get_name(cls, name):
        return cls.query.filter_by(name=name).first()
    
    @classmethod
    def get_all(cls):
        return cls.query.all()


class Employees(db.Model):
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    manager_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    
    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}
       
    def save(self):
        db.session.add(self)
        db.session.commit()
       
    @classmethod
    def get(cls, id):
        return cls.query.get(id)
    
    @classmethod
    def get_all(cls):
        return cls.query.all()

class Status(db.Model):
    __table_args__ = {'extend_existing': True}
    status = db.Column(db.String(100), primary_key=True)
    tasks = db.relationship('Tasks', backref='task_status', lazy='dynamic')

class Tasks(db.Model):
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    description = db.Column(db.String(1000))
    deadline = db.Column(db.Date)
    status = db.Column(db.String(100), db.ForeignKey('status.status'))
    assigned = db.Column(db.String(100), db.ForeignKey('user.name'))
    created_at = db.Column(db.DateTime)
    updated_on = db.Column(db.DateTime)
    is_deleted = db.Column(db.Boolean)
    
    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def save(self):
        db.session.add(self)
        db.session.commit()
       
    @classmethod
    def get(cls, id):
        return cls.query.get(id)
    
    @classmethod
    def get_all(cls):
        return cls.query.all()