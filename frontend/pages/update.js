import Head from "next/head"
import styles from "../styles/Profile.module.css"
import { useState } from "react"
import cookies from "next-cookies"

export async function getServerSideProps(context) {
	const { Authorization } = cookies(context)
	const res = await fetch(`http://localhost:5000/update`, {
		headers: { Authorization: Authorization },
	})
	if (res.status != 200) {
		return {
			props: {
				is_authenticated: false,
			},
		}
	}
	const data = await res.json()
	return {
		props: {
			is_authenticated: true,
			user: data.user,
		},
	}
}

export default function Update({ is_authenticated, user }, context) {
	const { Authorization } = cookies(context)
	const [email, setEmail] = useState(user.email)
	const [name, setName] = useState(user.name)
	const [contact, setContact] = useState(user.contact_no)
	const [showMessage, setShowMessage] = useState(false)
	const [invalidEmail, setInvalidEmail] = useState(false)
	const [invalidName, setInvalidName] = useState(false)
	const [invalidContact, setInvalidContact] = useState(false)
	function handleSubmit(e) {
		e.preventDefault()
		if (!ValidateEmail(email)) {
			setInvalidEmail(true)
			return
		} else {
			setInvalidEmail(false)
		}
		if (!ValidateName(name)) {
			setInvalidName(true)
			return
		} else {
			setInvalidName(false)
		}
		if (!ValidatePhone(contact)) {
			setInvalidContact(true)
			return
		} else {
			setInvalidContact(false)
		}
		setShowMessage(false)
		var formData = new FormData()
		formData.append("email", email)
		formData.append("name", name)
		formData.append("contact", contact)
		fetch("http://localhost:5000/update", {
			method: "POST",
			body: formData,
			headers: { Authorization: Authorization },
		})
			.then((res) => {
				return res
			})
			.then((res) => {
				// console.log(res)
				if (res.status == 200) {
					window.location = "/profile"
				} else setShowMessage(true)
				return res
			})
	}
	function ValidateEmail(mail) {
		return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
			mail
		)
	}

	function ValidatePhone(phone) {
		return /^[0-9]{10,12}$/.test(phone)
	}

	function ValidateName(name) {
		return name.trim() != ""
	}
	return (
		<div className={styles.container}>
			<Head>
				<title>Employee Management System</title>
				<meta name='description' content='CRUD and Authentication App' />
				<link rel='icon' href='/favicon.ico' />
				<link
					rel='stylesheet'
					href='https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css'
				/>
			</Head>

			<section className='hero is-primary is-fullheight'>
				<div className='hero-head'>
					<nav className='navbar'>
						<div className='container'>
<a role="button" class="navbar-burger" data-target="navMenu" aria-label="menu" aria-expanded="false">
								<span aria-hidden="true"></span>
								<span aria-hidden="true"></span>
								<span aria-hidden="true"></span>
							</a>
							<div id='navbarMenuHeroA' className='navbar-menu is-active'>
								<div className='navbar-end'>
									<a href="/" className='navbar-item'>
										Home
									</a>
									{is_authenticated && (
										<a href='/profile' className='navbar-item'>
											Profile
										</a>
									)}
									{is_authenticated && (
										<a href='/tasks' className='navbar-item'>
											Tasks
										</a>
									)}
									{!is_authenticated && (
										<a href='/login' className='navbar-item'>
											Login
										</a>
									)}
									{!is_authenticated && (
										<a href='/signup' className='navbar-item'>
											Sign Up
										</a>
									)}
									{is_authenticated && (
										<a href='/logout' className='navbar-item'>
											Logout
										</a>
									)}
								</div>
							</div>
						</div>
					</nav>
				</div>

				<div className='hero-body'>
					<div
						className={
							"container has-text-centered " + styles.overridecontainer
						}>
						<div className='column is-6 is-offset-3'>
							<h3 className='title'>Update your profile</h3>
							<div className='box'>
								{showMessage && (
									<div className='notification is-danger'>Invalid details</div>
								)}
								<form onSubmit={handleSubmit}>
									<div class='field'>
										<div class='control'>
											<input
												class='input is-large'
												type='email'
												value={email}
												name='email'
												placeholder='Your Email'
												autofocus=''
												onChange={(e) => {
													setEmail(e.target.value)
												}}
											/>
											{invalidEmail && (
												<p className={styles.dangerText}>
													You have entered an invalid email address!
												</p>
											)}
										</div>
									</div>

									<div class='field'>
										<div class='control'>
											<input
												class='input is-large'
												type='text'
												name='name'
												value={name}
												placeholder='Your Name'
												onChange={(e) => {
													setName(e.target.value)
												}}
											/>
											{invalidName && (
												<p className={styles.dangerText}>
													Please enter a proper name!
												</p>
											)}
										</div>
									</div>

									<div className='field'>
										<div className='control'>
											<input
												className='input is-large'
												type='tel'
												name='contact'
                                                value={contact}
												placeholder='Contact No'
												autofocus=''
												onChange={(e) => {
													setContact(e.target.value)
												}}
											/>
											{invalidContact && (
												<p className={styles.dangerText}>
													Please enter a proper 10 or 12 digit contact number!
												</p>
											)}
										</div>
									</div>
									<button class='button is-block is-info is-large is-fullwidth'>
										Update Profile
									</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	)
}
