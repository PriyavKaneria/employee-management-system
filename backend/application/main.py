from flask import Blueprint, render_template, redirect, url_for
from flask.globals import request
# from flask_login import login_required, current_user
from .models.models import User, Tasks, Employees
from datetime import datetime as dt
from .app import db
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required

main = Blueprint('main', __name__)

@main.route('/')
@jwt_required()
def index():
    # return render_template('index.html')
    user_id = get_jwt_identity()
    is_authenticated = False
    if User.get(user_id):
        is_authenticated = True
    return {"is_authenticated": is_authenticated}

@main.route('/profile')
@jwt_required()
def profile():
    users = User.get_all()
    ser_users = []
    for user in users:
        ser_users.append(user.as_dict())
    user_id = get_jwt_identity()
    user = User.get(user_id)
    # return render_template('profile.html', name=user.name, users=users)
    return {"name": user.name, "users": ser_users}


@main.route('/task/<int:task_id>')
@jwt_required()
def task(task_id):
    task = Tasks.get(task_id)
    ser_task = task.as_dict()
    user_id = User.get_name(ser_task['assigned']).id
    ser_task["user_id"] = user_id
    return {"task": ser_task}

@main.route('/tasks')
@jwt_required()
def tasks():
    user_id = get_jwt_identity()
    user = User.get(user_id)
    if user.role == "Admin":
        is_admin = True
        tasks = Tasks.get_all()
        ser_tasks = []
        for task in tasks:
            if not task.is_deleted:
                ser_tasks.append(task.as_dict())
    elif user.role == "Manager":
        is_admin = False
        employees = Employees.get_all()
        employee_list = []
        for employee in employees:
            if employee.manager_id == user_id:
                employee_list.append(User.get(employee.id).name)
        tasks = Tasks.get_all()
        ser_tasks = []
        for task in tasks:
            if task.assigned in employee_list and not task.is_deleted:
                ser_tasks.append(task.as_dict())
    else:
        is_admin = False
        tasks = Tasks.get_all()
        ser_tasks = []
        for task in tasks:
            if task.assigned == user.name and not task.is_deleted:
                ser_tasks.append(task.as_dict())
    return {"is_admin": is_admin, "tasks": ser_tasks}


@main.route('/tasks', methods=['POST'])
@jwt_required()
def newtasks():
    user_id = get_jwt_identity()
    user = User.get(user_id)
    title = request.form.get('title')
    description = request.form.get('description')
    deadline = request.form.get('deadline')
    status = request.form.get('status')
    assigned = request.form.get('employee')

    deadline = dt.strptime(deadline, "%Y-%m-%d")
    assigned = User.get(assigned).name

    if user.role == "Admin":
        newtask = Tasks(title=title, description=description, deadline=deadline, status=status, assigned=assigned, created_at=dt.now(), updated_on = dt.now(), is_deleted=False)
        newtask.save()
    else:
        return {"status_code": 401, "message": "Not authourized to create tasks"}
    db.session.commit()
    return {"status_code":200, "message":"Successfully made a new task"}

@main.route("/changestatus",methods=["POST"])
@jwt_required()
def changestatus():
    # user_id = get_jwt_identity()
    task_id = request.form.get('task_id')
    task = Tasks.get(task_id)
    task.status = request.form.get('status')
    task.updated_on = dt.now()
    db.session.commit()
    return {"status_code": 200, "message":"Successfully updated task"}

@main.route("/deletetask",methods=["POST"])
@jwt_required()
def deletetask():
    # user_id = get_jwt_identity()
    task_id = request.form.get('task_id')
    task = Tasks.get(task_id)
    task.is_deleted = True
    task.updated_on = dt.now()
    db.session.commit()
    return {"status_code": 200, "message":"Successfully updated task"}

@main.route('/updatetask', methods=['POST'])
@jwt_required()
def updatetask():
    task_id = request.form.get('task_id')
    task = Tasks.get(task_id)
    task.title = request.form.get('title')
    task.description = request.form.get('description')
    deadline = request.form.get('deadline')
    task.status = request.form.get('status')
    assigned = request.form.get('employee')
    task.updated_on = dt.now()

    task.deadline = dt.strptime(deadline, "%Y-%m-%d")
    task.assigned = User.get(assigned).name
    db.session.commit()
    return {"status_code":200, "message":"Successfully updated task"}

@main.route('/update')
@jwt_required()
def profileupdate():
    user_id = get_jwt_identity()
    user = User.get(user_id)
    # return render_template('update.html', user=user)
    return {"user": user.as_dict()}
    
    
@main.route('/update', methods=["POST"])
@jwt_required()
def profileupdatepost():
    user_id = get_jwt_identity()
    user = User.get(user_id)
    user.updated_on = dt.now()
    user.name = request.form.get('name')
    user.email = request.form.get('email')
    user.contact_no = request.form.get('contact')
    db.session.commit()
    # return redirect(url_for('main.profile'))
    return {"message": "Updated", "status_code": 200}

@main.route('/activate/<int:id>')
@jwt_required()
def activate(id):
    user = User.get(id)
    user.updated_on = dt.now()
    user.is_active = True
    db.session.commit()
    # return redirect(url_for('main.profile'))
    return {"message": "Activated", "status_code": 200}

@main.route('/deactivate/<int:id>')
@jwt_required()
def deactivate(id):
    user = User.get(id)
    user.updated_on = dt.now()
    user.is_active = False
    db.session.commit()
    # return redirect(url_for('main.profile'))
    return {"message": "Deactivated", "status_code": 200}

# from app import db, create_app
# db.create_all(app=create_app())