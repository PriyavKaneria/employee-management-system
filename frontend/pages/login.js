import Head from "next/head"
import styles from "../styles/Home.module.css"
import { useState } from "react"
import cookies from "next-cookies"

export async function getServerSideProps(context) {
	const { Authorization } = cookies(context)
	const res = await fetch(`http://localhost:5000/`, {
		headers: { Authorization: Authorization },
	})
	if (res.status != 200) {
		return {
			props: {
				is_authenticated: false,
			},
		}
	}
	return {
		props: {
			is_authenticated: true,
		},
	}
}

export default function Login({ is_authenticated }) {
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
    const [remember, setRemember] = useState(false)
	const [showMessage, setShowMessage] = useState(false)
	function handleSubmit(e) {
		e.preventDefault()
		setShowMessage(false)
		var formData = new FormData()
		formData.append("email", email)
		formData.append("password", password)
		formData.append("remember", remember)
		fetch("http://localhost:5000/login", {
			method: "POST",
            credentials: 'include',
			body: formData,
		})
			.then((res) => {
                return(res)
			})
            .then((res) => {
                console.log(res);
				if (res.status==200) {window.location = "/profile"}
                else setShowMessage(true)
				return res
            })
	}
	return (
		<div className={styles.container}>
			<Head>
				<title>Employee Management System</title>
				<meta name='description' content='CRUD and Authentication App' />
				<link rel='icon' href='/favicon.ico' />
				<link
					rel='stylesheet'
					href='https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css'
				/>
			</Head>

			<section className='hero is-primary is-fullheight'>
				<div className='hero-head'>
					<nav className='navbar'>
						<div className='container'>
<a role="button" class="navbar-burger" data-target="navMenu" aria-label="menu" aria-expanded="false">
								<span aria-hidden="true"></span>
								<span aria-hidden="true"></span>
								<span aria-hidden="true"></span>
							</a>
							<div id='navbarMenuHeroA' className='navbar-menu is-active'>
								<div className='navbar-end'>
									<a href="/" className='navbar-item'>
										Home
									</a>
									{is_authenticated && (
										<a href='/profile' className='navbar-item'>
											Profile
										</a>
									)}
									{is_authenticated && (
										<a href='/tasks' className='navbar-item'>
											Tasks
										</a>
									)}
									{!is_authenticated && (
										<a href='/login' className='navbar-item'>
											Login
										</a>
									)}
									{!is_authenticated && (
										<a href='/signup' className='navbar-item'>
											Sign Up
										</a>
									)}
									{is_authenticated && (
										<a href='/logout' className='navbar-item'>
											Logout
										</a>
									)}
								</div>
							</div>
						</div>
					</nav>
				</div>

				<div className='hero-body'>
					<div className='container has-text-centered'>
						<div className='column is-4 is-offset-4'>
							<h3 className='title'>Log In</h3>
							<div className='box'>
								{showMessage && (
									<div className='notification is-danger'>
										Invalid details or Inactive account!
									</div>
								)}
								<form onSubmit={handleSubmit}>
									<div class='field'>
										<div class='control'>
											<input
												class='input is-large'
												type='email'
												name='email'
												placeholder='Your Email'
												autofocus=''
												required
												onChange={(e) => {
													setEmail(e.target.value)
												}}
											/>
										</div>
									</div>

									<div class='field'>
										<div class='control'>
											<input
												class='input is-large'
												type='password'
												name='password'
												placeholder='Your Password'
												required
												onChange={(e) => {
													setPassword(e.target.value)
												}}
											/>
										</div>
									</div>
									<div class='field'>
										<label class='checkbox'>
											<input
												type='checkbox'
												name='remember'
												onChange={(e) => {
													setRemember(e.target.value)
												}}
											/>
											Remember me
										</label>
									</div>
									<button class='button is-block is-info is-large is-fullwidth'>
										Login
									</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	)
}
