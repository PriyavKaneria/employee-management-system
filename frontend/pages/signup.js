import Head from "next/head"
import styles from "../styles/Home.module.css"
import { useState } from "react"
import cookies from "next-cookies"

export async function getServerSideProps(context) {
	const { Authorization } = cookies(context)
	const res = await fetch(`http://localhost:5000/`, {
		headers: { Authorization: Authorization },
	})
	if (res.status != 200) {
		return {
			props: {
				is_authenticated: false,
			},
		}
	}
	return {
		props: {
			is_authenticated: true,
		},
	}
}

export default function SignUp({ is_authenticated }) {
	const [email, setEmail] = useState("")
	const [name, setName] = useState("")
	const [contact, setContact] = useState("")
	const [role, setRole] = useState("Admin")
	const [password, setPassword] = useState("")
	const [showMessage, setShowMessage] = useState(false)
	const [invalidEmail, setInvalidEmail] = useState(false)
	const [invalidName, setInvalidName] = useState(false)
	const [invalidContact, setInvalidContact] = useState(false)
	const [employees, setEmployees] = useState([])
	const [selected_employees, setSelectedEmployees] = useState([])
	const [managers, setManagers] = useState([])
	const [manager, setManager] = useState("")

	function handleRole(role) {
		setRole(role)
		if (role == "Manager") {
			fetch("http://localhost:5000/employees").then((res)=>res.json()).then(
				(res)=> {
					setEmployees(res.users)
				}
			)
		} else if (role == "Employee") {
			fetch("http://localhost:5000/managers").then((res)=>res.json()).then(
				(res)=> {
					setManagers(res.users)
				}
			)
		}
	}
	function handleSubmit(e) {
		e.preventDefault()
		if (!ValidateEmail(email)) {
			setInvalidEmail(true)
			return
		} else {
			setInvalidEmail(false)
		}
		if (!ValidateName(name)) {
			setInvalidName(true)
			return
		} else {
			setInvalidName(false)
		}
		if (!ValidatePhone(contact)) {
			setInvalidContact(true)
			return
		} else {
			setInvalidContact(false)
		}
		setShowMessage(false)
		var formData = new FormData()
		formData.append("email", email)
		formData.append("name", name)
		formData.append("contact", contact)
		formData.append("role", role)
		formData.append("employees", selected_employees)
		formData.append("manager", manager)
		formData.append("password", password)
		console.log(role, selected_employees, manager);
		fetch("http://localhost:5000/signup", {
			method: "POST",
			credentials: "include",
			body: formData,
		})
			.then((res) => {
				return res.json()
			})
			.then((res) => {
				if (res.status_code == 200) {
					window.location = "/login"
				}else setShowMessage(true)
				return res
			})
	}
	function ValidateEmail(mail) {
		return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
			mail
		)
	}

	function ValidatePhone(phone) {
		return /^[0-9]{10,12}$/.test(phone)
	}

	function ValidateName(name) {
		return name.trim() != ""
	}
	return (
		<div className={styles.container}>
			<Head>
				<title>Employee Management System</title>
				<meta name='description' content='CRUD and Authentication App' />
				<link rel='icon' href='/favicon.ico' />
				<link
					rel='stylesheet'
					href='https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css'
				/>
			</Head>

			<section className='hero is-primary is-fullheight'>
				<div className='hero-head'>
					<nav className='navbar'>
						<div className='container'>
							<a
								role='button'
								class='navbar-burger'
								data-target='navMenu'
								aria-label='menu'
								aria-expanded='false'>
								<span aria-hidden='true'></span>
								<span aria-hidden='true'></span>
								<span aria-hidden='true'></span>
							</a>
							<div id='navbarMenuHeroA' className='navbar-menu is-active'>
								<div className='navbar-end'>
									<a href='/' className='navbar-item'>
										Home
									</a>
									{is_authenticated && (
										<a href='/profile' className='navbar-item'>
											Profile
										</a>
									)}
									{is_authenticated && (
										<a href='/tasks' className='navbar-item'>
											Tasks
										</a>
									)}
									{!is_authenticated && (
										<a href='/login' className='navbar-item'>
											Login
										</a>
									)}
									{!is_authenticated && (
										<a href='/signup' className='navbar-item'>
											Sign Up
										</a>
									)}
									{is_authenticated && (
										<a href='/logout' className='navbar-item'>
											Logout
										</a>
									)}
								</div>
							</div>
						</div>
					</nav>
				</div>

				<div className='hero-body'>
					<div className='container has-text-centered'>
						<div className='column is-4 is-offset-4'>
							<h3 className='title'>Sign Up</h3>
							<div className='box'>
								{showMessage && (
									<div className='notification is-danger'>
										Invalid details! Could not sign up!
									</div>
								)}
								<form onSubmit={handleSubmit}>
									<div className='field'>
										<div className='control'>
											<input
												className='input is-large'
												type='email'
												name='email'
												placeholder='Email'
												autofocus=''
												onChange={(e) => {
													setEmail(e.target.value)
												}}
											/>
											{invalidEmail && (
												<p className={styles.dangerText}>
													You have entered an invalid email address!
												</p>
											)}
										</div>
									</div>

									<div className='field'>
										<div className='control'>
											<input
												className='input is-large'
												type='text'
												name='name'
												placeholder='Name'
												autofocus=''
												onChange={(e) => {
													setName(e.target.value)
												}}
											/>
											{invalidName && (
												<p className={styles.dangerText}>
													Please enter a proper name!
												</p>
											)}
										</div>
									</div>

									<div className='field'>
										<div className='control'>
											<input
												className='input is-large'
												type='tel'
												name='contact'
												placeholder='Contact No'
												autofocus=''
												onChange={(e) => {
													setContact(e.target.value)
												}}
											/>
											{invalidContact && (
												<p className={styles.dangerText}>
													Please enter a proper 10 or 12 digit contact number!
												</p>
											)}
										</div>
									</div>

									<div className='field'>
										<div className='control'>
											<div class='select is-large is-fullwidth'>
												<select
													onChange={(e) => {
														handleRole(e.target.value)
													}}>
													<option>Admin</option>
													<option>Manager</option>
													<option>Employee</option>
												</select>
											</div>
										</div>
									</div>

									{role === "Manager" && (
										<div className='field'>
											<div className='control'>
												<div className='select is-multiple is-large is-fullwidth'>
													<select multiple size='1' onChange={(e)=>{setSelectedEmployees(e.target.value)}}>
														{employees.map((emp) => {
															return <option value={emp.id}>{emp.name}</option>
														})}
													</select>
												</div>
											</div>
										</div>
									)}

									{role === "Employee" && (
										<div className='field'>
											<div className='control'>
												<div className='select is-large is-fullwidth'>
													<select onChange={(e)=>{setManager(e.target.val)}}>
														<option value="">None</option>
														{managers.map((man) => {
															return <option value={man.id}>{man.name}</option>
														})}
													</select>
												</div>
											</div>
										</div>
									)}

									<div className='field'>
										<div className='control'>
											<input
												className='input is-large'
												type='password'
												name='password'
												placeholder='Password'
												onChange={(e) => {
													setPassword(e.target.value)
												}}
											/>
										</div>
									</div>

									<button className='button is-block is-info is-large is-fullwidth'>
										Sign Up
									</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	)
}
